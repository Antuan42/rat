﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSController : MonoBehaviour
{

    private CharacterController MyControl;
    public Animator Anim;

    public float Speed = 12f;
    public float Gravity = -9.81f;
    public float JumpHeight = 3f;

    public Transform groundCheck;
    public float GroundDistance = 0.2f;
    public LayerMask GroundMask;

    Vector3 velocity;
    bool isGrounded;

    private int Points;
    public Text PointsText;

    // Start is called before the first frame update
    void Start()
    {
        MyControl = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Anim.SetFloat("Run", Mathf.Abs(Input.GetAxis("Horizontal")) + Mathf.Abs(Input.GetAxis("Vertical")));
        PointsText.text = Points.ToString();

        isGrounded = Physics.CheckSphere(groundCheck.position, GroundDistance, GroundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        MyControl.Move(move * Speed * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
        }

        velocity.y += Gravity * Time.deltaTime;

        MyControl.Move(velocity * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Points")
        {
            Points += 10;
            Destroy(other.gameObject);
        }
    }
}
